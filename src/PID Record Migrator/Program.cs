﻿using System.Web;
using Coscine.Api.Core.Shared;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;
using Microsoft.Extensions.Configuration;
using Winton.Extensions.Configuration.Consul;

var _dummyMode = true;
_dummyMode = !(args.Length > 0 && args[0] == "--noDryRun");

// Console text output
Console.Write($"{new string('=', 80)}\n PID Record Migrator");

if (_dummyMode)
{
    Console.Write(" : DUMMY MODE");
}
Console.WriteLine($"\n{new string('-', 80)}");

var configBuilder = new ConfigurationBuilder();

// Define the Consul URL
var consulUrl = Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

// Remove the default sources
configBuilder.Sources.Clear();

var configuration = configBuilder
    .AddConsul(
        "coscine/Coscine.Api/appsettings",
        options =>
        {
            options.ConsulConfigurationOptions = cco => cco.Address = new Uri(consulUrl);
            options.Optional = true;
            options.ReloadOnChange = true;
            options.PollWaitTime = TimeSpan.FromSeconds(5);
            options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
        }
    )
    .Build();

var apiConfiguration = new Configuration()
{
    BasePath = "http://localhost:7206/coscine",
    ApiKeyPrefix = { { "Authorization", "Bearer" } },
    ApiKey =
    {
        {
            "Authorization",
            ApiConfigurationUtil.GenerateAdminToken(ApiConfigurationUtil.RetrieveJwtConfiguration())
        }
    },
};

var pidApi = new PidApi(apiConfiguration);

Console.WriteLine($"- Collecting Coscine data ...");

var pids = await RequestUtil.WrapPagedRequest<PidDtoPagedResponse, PidDto>(
    (currentPage) =>
        pidApi.GetPidsAsync(
            includeProjects: true,
            includeResources: true,
            includeDeleted: false,
            pageNumber: currentPage,
            pageSize: 250
        )
);
var proxyUrlString =
    configuration.GetSection("ConnectionConfiguration").GetSection("ProxyUrl").Value
    ?? throw new Exception("ConnectionConfiguration:ProxyUrl is not defined in the configuration!");
var digitalObjectLocationUrlString =
    configuration.GetSection("PidConfiguration").GetSection("digitalObjectLocationUrl").Value
    ?? throw new Exception(
        "PidConfiguration:DigitalObjectLocationUrl is not defined in the configuration!"
    );

Console.WriteLine($"- Search yielded {pids.Count()} PID(s)");

var handleApi = new HandleApi(apiConfiguration);
var resourceApi = new ResourceApi(apiConfiguration);
var projectApi = new ProjectApi(apiConfiguration);
int updatedPidCount = 0;

Console.WriteLine($"- Iterating through {pids.Count()} PID(s) ...");
foreach (var pid in pids)
{
    // Check if there's an entry in the handle service
    try
    {
        Console.WriteLine("- Find the PID in the handle service ...");
        var handle = (await handleApi.GetHandleAsync(pid.Prefix, pid.Suffix)).Data;
    }
    catch (Exception e)
    {
        Console.WriteLine(
            $"└ The following PID: {pid.Suffix} could not be found in the handle service.\n {e.Message}\n"
        );
        continue;
    }
    // Only update when PIDs are available
    if (!_dummyMode)
    {
        Console.WriteLine($"- Updating Coscine PID ...");

        try
        {
            // Check if project or resource
            if (pid.Type.ToString()?.Equals("resource", StringComparison.OrdinalIgnoreCase) == true)
            {
                try
                {
                    // Generate the PID record
                    var resourceHandleValues = await GenerateResourceHandleValuesAsync(
                        pid.Prefix,
                        Guid.Parse(pid.Suffix)
                    );
                    // Send the update with the request content
                    await handleApi.UpdateHandleAsync(
                        pid.Prefix,
                        pid.Suffix,
                        new HandleForUpdateDto { Values = resourceHandleValues.ToList() }
                    );
                    updatedPidCount++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"└ Updating the following PID: {pid.Suffix} FAILED.\n {e.Message}\n"
                    );
                }
            }
            if (pid.Type.ToString()?.Equals("project", StringComparison.OrdinalIgnoreCase) == true)
            {
                try
                {
                    // Generate the PID record
                    var projectHandleValues = await GenerateProjectHandleValuesAsync(
                        pid.Prefix,
                        Guid.Parse(pid.Suffix)
                    );
                    // Send the update with the request content
                    await handleApi.UpdateHandleAsync(
                        pid.Prefix,
                        pid.Suffix,
                        new HandleForUpdateDto { Values = projectHandleValues.ToList() }
                    );
                    updatedPidCount++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"└ Updating the following PID: {pid.Suffix} FAILED.\n {e.Message}\n"
                    );
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(
                $"└ Updating the following PID: {pid.Suffix} FAILED.\n {e.Message}\n"
            );
            Console.WriteLine($"└ Migration FAILED. {e.Message}");
            return;
        }
    }
}
Console.WriteLine($"Updated {updatedPidCount} Coscine PIDs\n");
Console.WriteLine($"Finished");

async Task<IEnumerable<HandleValueForUpdateDto>> GenerateProjectHandleValuesAsync(
    string prefix,
    Guid suffix
)
{
    var handles = new List<HandleValueForUpdateDto>();

    // Use the configuration to build necessary URLs and prefixes
    var pid = $"{prefix.Trim('/')}/{suffix}";
    var baseUri = new Uri(proxyUrlString, UriKind.Absolute);
    var digitalObjectLocationUri = new Uri(digitalObjectLocationUrlString, UriKind.Absolute);
    var projectDto = (await projectApi.GetProjectAsync(suffix.ToString())).Data;
    var isProjectPublic =
        projectDto?.Visibility?.DisplayName.Equals("public", StringComparison.OrdinalIgnoreCase)
        ?? false;

    var idx = 1;

    // Create the URL handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = "URL",
            ParsedData = new Uri(baseUri, $"/pid/?pid={HttpUtility.UrlEncode(pid)}")
        }
    );

    // Create the kernel information profile handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.KernelInformationProfileHandle,
            ParsedData = PidHandles.CoscineKernelInformationProfileHandle
        }
    );

    // Create the date created handle value
    if (projectDto.CreationDate is not null && isProjectPublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.DateCreatedHandle,
                ParsedData = projectDto.CreationDate
            }
        );
    }

    // Create the digital object location handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectLocationHandle,
            ParsedData = new Uri(
                digitalObjectLocationUri,
                $"/coscine/api/v2/projects/{projectDto.Id}"
            )
        }
    );

    // Create the digital object type handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectTypeHandle,
            ParsedData = PidHandles.DigitalObjectTypeProjectHandle
        }
    );

    // Create the topic (discipline) handle value
    var disciplines = projectDto.Disciplines.Where(pd => pd.DisplayNameEn is not null).ToList();
    if (disciplines.Count != 0 && isProjectPublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.TopicHandle,
                // NOTE: Consider adding all disciplines as a comma-separated string
                ParsedData = disciplines[0].DisplayNameEn ?? string.Empty
            }
        );
    }

    // Create the contact (organizations) handle value
    if (projectDto.Organizations.Count != 0 && isProjectPublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.ContactHandle,
                // NOTE: Consider adding all institutes as a comma-separated string
                ParsedData = projectDto.Organizations[0].Uri
            }
        );
    }

    return handles;
}

async Task<IEnumerable<HandleValueForUpdateDto>> GenerateResourceHandleValuesAsync(
    string prefix,
    Guid suffix
)
{
    var handles = new List<HandleValueForUpdateDto>();

    // Use the configuration to build necessary URLs and prefixes
    var pid = $"{prefix.Trim('/')}/{suffix}";
    var baseUri = new Uri(proxyUrlString, UriKind.Absolute);
    var digitalObjectLocationUri = new Uri(digitalObjectLocationUrlString, UriKind.Absolute);
    var resourceDto = (await resourceApi.GetResourceAsync(suffix)).Data;
    var isResourcePublic =
        resourceDto?.Visibility?.DisplayName.Equals("public", StringComparison.OrdinalIgnoreCase)
        ?? false;

    var idx = 1;

    // Create the URL handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = "URL",
            ParsedData = new Uri(baseUri, $"/pid/?pid={HttpUtility.UrlEncode(pid)}")
        }
    );

    // Create the kernel information profile handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.KernelInformationProfileHandle,
            ParsedData = PidHandles.CoscineKernelInformationProfileHandle
        }
    );

    // Create the date created handle value
    if (resourceDto.DateCreated is not null && isResourcePublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.DateCreatedHandle,
                ParsedData = resourceDto.DateCreated
            }
        );
    }

    // Create the digital object location handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectLocationHandle,
            ParsedData = new Uri(
                digitalObjectLocationUri,
                $"/coscine/api/v2/resources/{resourceDto.Id}"
            )
        }
    );

    // Create the digital object type handle value
    handles.Add(
        new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectTypeHandle,
            ParsedData = PidHandles.DigitalObjectTypeResourceHandle
        }
    );

    // Create the topic (discipline) handle value
    var disciplines = resourceDto.Disciplines.Where(rd => rd.DisplayNameEn is not null).ToList();
    if (disciplines.Count != 0 && isResourcePublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.TopicHandle,
                // NOTE: Consider adding all disciplines as a comma-separated string
                ParsedData = disciplines[0].DisplayNameEn ?? string.Empty
            }
        );
    }

    // Create the license handle value
    if (resourceDto.License?.Url is not null && isResourcePublic)
    {
        handles.Add(
            new()
            {
                Idx = idx++,
                Type = PidHandles.LicenseHandle,
                ParsedData = resourceDto.License.Url
            }
        );
    }

    return handles;
}

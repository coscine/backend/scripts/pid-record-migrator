﻿namespace Coscine.Api.Core.Shared;

/// <summary>
/// Contains Handles relevant to the Coscine PID Record.
/// All values are entries in the DTR:https://dtr-test.pidconsortium.eu/#objects/21.T11148/8882327b7c25331e3cdd
/// </summary>
public static class PidHandles
{   // The general handle of the Kernel Information Profile type
    public static readonly string KernelInformationProfileHandle = "21.T11148/076759916209e5d62bd5";

    // The handle of the Coscine Kernel Information Profile type
    public static readonly string CoscineKernelInformationProfileHandle = "21.T11148/8882327b7c25331e3cdd";

    // The handle of the date created type
    public static readonly string DateCreatedHandle = "21.T11148/aafd5fb4c7222e2d950a";

    // The handle of the digital object location type
    public static readonly string DigitalObjectLocationHandle = "21.T11148/b8457812905b83046284";

    // The handle of the digital object type
    public static readonly string DigitalObjectTypeHandle = "21.T11148/1c699a5d1b4ad3ba4956";

    // The handle of the digital object value: resource
    // DTR entry: https://dtr-test.pidconsortium.eu/#objects/21.T11148/12aad485b74d04f584c1
    public static readonly string DigitalObjectTypeResourceHandle = "21.T11148/12aad485b74d04f584c1";

    // The handle of the digital object value: project
    // DTR entry: https://dtr-test.pidconsortium.eu/#objects/21.T11148/0f13b0a83bd926fe269f
    public static readonly string DigitalObjectTypeProjectHandle = "21.T11148/0f13b0a83bd926fe269f";

    // The handle of the license type
    public static readonly string LicenseHandle = "21.T11148/2f314c8fe5fb6a0063a8";

    // The handle of the contact type
    public static readonly string ContactHandle = "21.T11148/1a73af9e7ae00182733b";

    // The handle of the topic type
    public static readonly string TopicHandle = "21.T11148/b415e16fbe4ca40f2270";
}